package pl.sda.bmi;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Weight in kg: ");
        float weight = sc.nextFloat();
        System.out.println("Height in cm: ");
        int height = sc.nextInt() / 100;


        double BMI = weight / (height * height);

        if (BMI >= 18.5 || BMI < 24.9) {
            System.out.println("BMI optymalne");
        } else if (BMI < 18.5 || BMI >= 25.0){

            System.out.println("BMI nieoptymalne");
        }
    }



    }

